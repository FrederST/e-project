import { Component, OnInit } from '@angular/core';
import {
  NzCarouselFlipStrategy,
  NzCarouselTransformNoLoopStrategy,
  NZ_CAROUSEL_CUSTOM_STRATEGIES,
} from 'ng-zorro-antd/carousel';

@Component({
  selector: 'app-tecnologies',
  templateUrl: './tecnologies.component.html',
  styleUrls: ['./tecnologies.component.scss'],
  providers: [
    {
      provide: NZ_CAROUSEL_CUSTOM_STRATEGIES,
      useValue: [
        {
          name: 'transform-no-loop',
          strategy: NzCarouselTransformNoLoopStrategy,
        },
        { name: 'flip', strategy: NzCarouselFlipStrategy },
      ],
    },
  ],
})
export class TecnologiesComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
