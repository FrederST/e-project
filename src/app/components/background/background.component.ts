import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.scss'],
})
export class BackgroundComponent implements OnInit {
  isVisible = false;
  index = 0;
  indexFile = 0;
  disable = false;
  path = '';

  constructor() {}

  ngOnInit(): void {}

  onIndexChange(index: number): void {
    this.index = index;
  }

  showModal(indexFile: number): void {
    this.indexFile = indexFile;
    this.isVisible = true;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }
}
