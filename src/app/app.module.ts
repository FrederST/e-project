import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzModuleModule } from './modules/nz-module/nz-module.module';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { BackgroundComponent } from './components/background/background.component';
import { WorkComponent } from './components/work/work.component';
import { TecnologiesComponent } from './components/tecnologies/tecnologies.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PetsComponent } from './components/pets/pets.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    AboutMeComponent,
    BackgroundComponent,
    WorkComponent,
    TecnologiesComponent,
    PetsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzModuleModule,
    PdfViewerModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent],
})
export class AppModule {}
