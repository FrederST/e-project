import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { BackgroundComponent } from './components/background/background.component';
import { PetsComponent } from './components/pets/pets.component';
import { TecnologiesComponent } from './components/tecnologies/tecnologies.component';
import { WorkComponent } from './components/work/work.component';

const routes: Routes = [
  { path: 'about-me', component: AboutMeComponent },
  { path: 'career', component: BackgroundComponent },
  { path: 'experience', component: WorkComponent },
  { path: 'skills', component: TecnologiesComponent },
  { path: 'friends', component: PetsComponent },
  {
    path: '',
    redirectTo: 'about-me',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
